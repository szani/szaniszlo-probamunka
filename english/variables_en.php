<?php 
$lang = array();
$lang['account_details']                ='Account Details';
$lang['user_details']                   ='User Details';
$lang['billing_address']                ='Billing Address';
$lang['delivery_address']               ='Delivery Address';
$lang['customer_data']                  ='Customer Data';
$lang['contact_name']                   = 'Contact Person';
$lang['contact_email']                  = 'Email address';
$lang['username']                       = 'Username <span>(optional)</span>';
$lang['your-name']                      = 'Your name';
$lang['your-email']                     = 'Your email';
$lang['your-phone']                     = 'Phone number &nbsp;<span>(optional)</span>';
$lang['your-message']                   = 'Your message';
$lang['contact-us']                     = 'Contact Us';
$lang['send']                           = 'Send';
        
$lang['register-name']                  = 'Name of contact person';
$lang['register-email']                 = 'Email address';
$lang['register-username']              = 'Username <span>(optional)</span>:';
$lang['register-register']              = 'Register';
$lang['register-complete']              = 'You have completed your registration. Please, <a href="/en/login">sign in</a>. The password has been sent by email.';
$lang['register-or-continue-shopping']  = 'Please complete your registration below or <a href=/en/all-products>continue shopping</a>.';
$lang['confirm-ok']                     = 'Your email has been confirmed.<br> ' . $lang['register-or-continue-shopping'];
$lang['registration-success-msg']       = 'Your registration was successful. A confirmation email has been sent to the given email address.<br> ' . $lang['register-or-continue-shopping'];
$lang['continue']                       = 'Continue';

$lang['bill-address']                   = 'Billing address';
$lang['bill-company-name']              = 'Company name';
$lang['bill-address-line1']             = 'Address line 1';
$lang['bill-address-line2']             = 'Address line 2 &nbsp;<span>(optional)</span>';
$lang['bill-city']                      = 'City';
$lang['bill-zip']                       = 'ZIP';
$lang['bill-same-address']              = 'Delivery address is the same.';
$lang['bill-phone']                     = 'Phone number';

$lang['not_available']                  = 'N/A';
$lang['customer_number']                = 'Customer Number';
$lang['customer-nr-invalid']            = 'Invalid customer number';
$lang['customer-nr-in-use']             = 'Customer number is in use';
$lang['discount']                       = 'Discount';
$lang['add_customer_number']            = 'Please add your Customer Number. 
                    <div class="aprobetu">If your customer number is not known, please call the hotline: +49&nbsp;(0)8571&nbsp;6028997. Thank you.
                    </div>
                    <br>'; //TBC

$lang['login-success-msg']              = 'Login successful.';
$lang['data-updated-msg']               = 'Data has been updated successfully.';

$lang['form-edit']                      = 'Edit';
$lang['form-update']                    = 'Update';
$lang['form-cancel']                    = 'Cancel';

$lang['form-company-name']              = ' Company name';
$lang['form-address']                   = ' Address line 1';
$lang['form-city']                      = ' City';
$lang['form-zip']                       = ' ZIP';

$lang['form-add-delivery']      = 'Add Delivery Address'; 
$lang['if-different-address']   = 'If your delivery address and billing address are not identical, plase, add your delivery address.'; //TBC

$lang['login-username']         = 'Username or email address';
$lang['login-password']         = 'Password';
$lang['login-login']            = 'Sign in';
$lang['login-error']            = 'Invalid username / email or password.';


$lang['order-thx']              = 'Thank your for ordering';
$lang['order-del-addr']         = 'Delivery address';


$lang['login-sgn-in-for-accoutn'] = 'Please, sign in for account editing';

$lang['search-result']          = 'Search result';
$lang['in-cart']                = '<a href="/en/shoppingcart">In the cart:';

$lang['summary']                = 'Order Summary';
$lang['edit-cart-content']      = '<a href="/en/shoppingcart">Edit cart content</a>';

$lang['email-confirmed']        = "Your email has been confirmed.<br>" . $lang['register-or-continue-shopping'];

$lang['forgot-password']        = '<a href="/en/password-reset">Forgot your password?</a>';
$lang['pw-reset']               = 'Password reset';
$lang['email-for-pw-reset']     = 'Please give your username or registered email address.';
$lang['reset-success-msg']      = 'Your request has been sent. Please, check your email account for the password-reset link.';
$lang['reset-error']            = 'Sorry, the given email address or username cannot be found in our database.';

$lang['new-password']           = 'New password';
$lang['repeat-password']        = 'Repeat password';

$lang['reset-link-error']       = 'Sorry, the password reset link is invalid.';
$lang['new-pwd-success-msg']    = 'You have successfully changed your password';

$lang['confirmation']           = 'Confirmation';

$lang['email-already-subscribed'] = 'The email address has already been subscribed.';
$lang['subscription-ok']        = 'You have successfully subscribed to our newsletter';
$lang['subscription-error']     = 'An error occured while sending your request.';

$lang['subscribe-on-register']  = 'Sign up for newsletter.';

$lang['cookie-text']            = 'This site uses cookies. By continuing to browse the site, you are agreeing to our use of cookies. For more information, please read our <a href="/en/privacy-policy">privacy policy</a>.';
$lang['hf_sign_up']='Sign up to newsletter';
$lang['hf_submit']='Submit';
$lang['hf_enter_email']='Enter your email address';
$lang['hf_online_order']='Online order';
$lang['hf_sign_in']='<a href="/en/login">Sign in</a>';
$lang['hf_sign_out']='Sign out';
$lang['hf_search']='Search';
$lang['hf_register']='Register';
$lang['menu_products']='<a href="/en/all-products">Products</a>';
$lang['menu_about_us']='<a href="/en/about-us">About Us</a>';
$lang['menu_contact']='<a href="/en/contact">Contact</a>';
$lang['menu_downloads']='Downloads';
$lang['menu_randd']='<a href="/en/research-and-development">Research & Development</a>';
$lang['menu_certificate']='Certificate';
$lang['menu_terms']='<a href="/en/terms-and-conditions">Terms and Conditions</a>';
$lang['menu_privacy']='<a href="/en/privacy-policy">Privacy Policy</a>';
$lang['menu_shipping']='<a href="/en/shipping">Shipping</a>';
$lang['menu_my_account']='My Account';
$lang['menu_home']='Home';
$lang['menu_general']='General';
$lang['menu_assistance']='Assistance';
$lang['confirmation-successful'] = 'The confirmation was successful. Please <a href="/en/login">Sign In</a>';
$lang['already-confirmed'] = 'The user has already been confirmed. Plesase, <a href="%s"> complete your registration </a>';

$lang['contact-thx'] = 'Tanks for your message, it has been sent successfully. We contact you soon.';
$lang['product-product'] = 'Product';
$lang['product-unitprice'] = 'Unitprice';
$lang['prod_add_cart']='Add to cart';
$lang['prod_content']='Content of package';
$lang['orderno'] = 'Order No';
$lang['total_pcs'] = 'total <br>pieces';
$lang['total_ml'] = 'total <br>quantity';
$lang['quantity'] = 'quantity';
$lang['subtotal'] = 'subtotal';
$lang['subtotal_abbr'] = 'subtotal';
$lang['added-cart-msg'] = 'Product added to cart. <a href="/en/shoppingcart">Go to cart</a> or <a href="/en/all-products">continue shopping</a>';
$lang['back-products'] = 'back to products';
$lang['proceed-summary'] = 'Go to order summary';
$lang['proceed-order'] = 'Proceed to order';
$lang['total'] = 'total';
$lang['shoppingcart'] = 'shoppingcart';
$lang['pieces'] = 'pieces';

$lang['other-packings'] = 'Available Packings';
$lang['ml'] = 'ml';
$lang['pcs'] = 'pieces';

/*indexek kiiratása

     print_r (array_keys($lang));
?><br><br><?php */

/*bejárjuk

 $hossz=(sizeof(array_keys($lang)));

for($i=0; $i<$hossz; $i++){
    echo str_replace("-","_",array_keys($lang)[$i]) .","."<br>";
}; */

//átalakítás sql paranccsá

        //minta
        //INSERT INTO `bilingual`.`variables` (`id`, `date_created`, `variable`) VALUES (NULL, CURRENT_TIMESTAMP, 'almafa'), 
        //(NULL, CURRENT_TIMESTAMP,'körtefa'),

/* $hossz=(sizeof(array_keys($lang)));

    for($i=0; $i<$hossz; $i++){
        echo "(NULL, CURRENT_TIMESTAMP, '".str_replace("-","_",array_keys($lang)[$i]) ."'),"."<br>";

}; */


//kulcsok kiiratása

/*print_r (array_values($lang));
?><br><br><?php 

//bejárjuk

 $hossz=(sizeof(array_values($lang)));

for($i=0; $i<$hossz; $i++){
    echo str_replace("-","_",array_values($lang)[$i]) .","."<br>";
}; */

//átalakítás sql paranccsá

        //minta
        //minta INSERT INTO `bilingual`.`variables_translation` (`id`, `variable_id`, `language_code`, `description`) VALUES (NULL, '1', 'en', 'Account Details'), 
        //(NULL, '2', 'en', 'User Details');

 $hossz=(sizeof(array_values($lang)));
 
for($i=0; $i<$hossz; $i++){
    echo "(NULL,".($i+1).",'en','".str_replace("-","_",array_values($lang)[$i]) ."'),"."<br>";
}; 


