<?php
$lang = array();
$lang['account_details']='Kontodaten';
$lang['user_details']='Benutzerdaten';
$lang['billing_address']='Rechnungsadresse';
$lang['delivery_address']='Lieferadresse';
$lang['customer_data']='Kundendaten';
$lang['contact_name'] = 'Anschprechpartner';
$lang['contact_email'] = 'E-Mail-Adresse';
$lang['username'] = 'Benutzername <span>(nicht verpflichtend)</span>';
$lang['your-name'] = 'Ihre Name';
$lang['your-email'] = 'Ihre E-Mail-Adresse';
$lang['your-phone'] = 'Ihre Telefon <span>(nicht notwendig)</span>';
$lang['your-message'] = 'Nachricht';
$lang['contact-us'] = 'Kontaktiere Uns';
$lang['send'] = 'Absenden';

$lang['register-name'] = 'Kontaktperson';
$lang['register-email'] = 'E-Mail-Adress';
$lang['register-username'] = 'Benutzername <span>(nicht notwendig)</span>';
$lang['register-register'] = 'Anmeldung';
$lang['register-complete'] = 'Die Registrierung war erfolgreich. Bitte, <a href="/login">melden Sie an</a>. Das Passwort wurde per E-Mail verschickt.';
$lang['register-or-continue-shopping'] = 'Füllen Sie bitte Ihre Anmeldung unten aus oder <a href=/all-products>gehen Sie mit dem Einkauf fortsetzen</a>.';
$lang['confirm-ok'] = 'Deine Email wurde bestätigt. <br> ' . $lang['register-or-continue-shopping'];
$lang['registration-success-msg'] = 'Ihre Anmeldung war erfolgreich. Eine Bestätigungs-E-Mail wurde an die angegebene E-Mail-Adresse gesendet.<br> ' . $lang['register-or-continue-shopping'];
$lang['continue'] = 'Weiter';

$lang['bill-address'] = 'Rechnungsadresse';
$lang['bill-company-name'] = 'Name der Firma';
$lang['bill-address-line1'] = 'Strasse, Nr.';
$lang['bill-address-line2'] = 'Address line 2 &nbsp;<span>(optional)</span>';
$lang['bill-city'] = 'Ort';
$lang['bill-zip'] = 'PLZ';
$lang['bill-same-address'] = 'Lieferadresse gleich der Rechnungsadresse.';
$lang['bill-phone'] = 'Telefon';

$lang['not_available'] = 'nicht verfügbar';
$lang['customer_number'] = 'Kundennumber';
$lang['customer-nr-invalid'] = 'Ungültige Kundennumber';
$lang['customer-nr-in-use'] = 'Kundennumber bereits vergeben'; //TBC
$lang['discount'] = 'Rabat';
$lang['add_customer_number'] = 'Fügen Sie bitte Ihren Kundennumber hinzu. 
                    <div class="aprobetu">Falls Ihnen Ihre Kundennummer nicht bekannt ist, rufen Sie unsere Hotline-Nr.: +49&nbsp;(0)8571&nbsp;6028997 an. Vielen Dank.</div>
                    <br>'; //translate

$lang['login-success-msg'] = 'Anmeldung erfolgreich';
$lang['data-updated-msg'] = 'Daten wurden erfolgreich aktualisiert.';

$lang['form-edit'] = 'Bearbeiten';
$lang['form-update'] = 'Aktualisieren';
$lang['form-cancel'] = 'Abbrechen';

$lang['form-company-name'] = ' Name der Firma';
$lang['form-address'] = ' Strasse, Nr.';
$lang['form-city'] = ' Ort';
$lang['form-zip'] = ' PLZ';

$lang['form-add-delivery'] = 'Lieferadresse hinzufügen'; //TBC
$lang['if-different-address'] = 'Wenn Lieferadresse nicht identisch mit Rechnungsadresse, bitte eine Lieferadresse hinzufügen.'; //TBC


$lang['login-username'] = 'Benutzername oder E-Mail-Adresse';
$lang['login-password'] = 'Passwort';
$lang['login-login'] = 'Einloggen';
$lang['login-error'] = 'Ungültiger Benutzername / E-mail oder passwort.';


$lang['order-thx'] = 'Vielen Dank für die Bestellung.';
$lang['order-del-addr'] = 'Lieferadresse';

$lang['login-sgn-in-for-accoutn'] = 'Bitte melden Sie sich für die Kontobearbeitung an';  //TBC

$lang['search-result'] = 'Suchergebnis';
$lang['in-cart'] = '<a href="/shoppingcart">Im warenkorb:';

$lang['summary'] = 'Bestellübersicht';
$lang['edit-cart-content'] = '<a href="/shoppingcart">Warenkorb Bearbeiten</a>';  //TBC

$lang['email-confirmed'] = "Ihre E-Mail-Adresse wurde bestätigt.<br>" . $lang['register-or-continue-shopping']; //TBC

$lang['forgot-password'] = '<a href="/password-reset">Haben Sie Ihr Passwort vergessen?</a>';
$lang['pw-reset'] = 'Passwort zurücksetzen'; //TBC
$lang['email-for-pw-reset'] = 'Bitte geben Sie Ihre Benutzername order registrierte E-Mail-Adresse an.'; //TBC

$lang['reset-success-msg'] = 'Your request has been sent. Please, check your email account for the password-reset link.'; //translate
$lang['reset-error'] = 'Sorry, the given email address or username cannot be found in our database.';  //translate

$lang['new-password'] = 'Neues Passwort';
$lang['repeat-password'] = 'Passwort Wiederholen'; //TBC

$lang['reset-link-error'] = 'Ungültiger passwort link.'; //TBC
$lang['new-pwd-success-msg'] = 'Sie haben Ihr Passwort erfolgreich geändert.'; //TBC

$lang['confirmation'] = 'Bestätigung';

$lang['email-already-subscribed'] = 'Die E-Mail-Adresse wurde bereits abonniert.'; //TBC
$lang['subscription-ok'] = 'Sie haben erfolgreich unseren Newsletter abonniert.'; //TBC
$lang['subscription-error'] = 'Beim Senden Ihrer Anfrage ist ein Fehler aufgetreten.'; //TBC

$lang['subscribe-on-register'] = 'Anmeldung für Newsletter.';

$lang['cookie-text'] = 'Diese Seite verwendet Cookies. Indem Sie die Website weiterhin durchsuchen, stimmen Sie unserer Verwendung von Cookies zu. Für weitere Informationen lesen Sie bitte unsere <a href="/datenschutzhinweise">Datenschutzbestimmungen</a>.';
$lang['hf_sign_up']='Newsletter abonnieren';
$lang['hf_submit']='Absenden';
$lang['hf_enter_email']='Ihre E-Mail-Adresse';
$lang['hf_online_order']='Online bestellen';
$lang['hf_sign_in']='<a href="/login">Einloggen</a>';
$lang['hf_sign_out']='Ausloggen';
$lang['hf_search']='Suche';
$lang['hf_register']='Anmeldung';
$lang['menu_products']='<a href="/all-products">Produkte</a>';
$lang['menu_about_us']='<a href="/uber-uns">Über uns</a>';
$lang['menu_contact']='<a href="/kontakt">Kontakt</a>';
$lang['menu_downloads']='Downloads';
$lang['menu_randd']='<a href="/forschung-und-entwicklung">Forschung und Entwicklung</a>';
$lang['menu_certificate']='Zertifikate';
$lang['menu_terms']='<a href="/agb">AGB</a>';
$lang['menu_privacy']='<a href="/datenschutzhinweise">Datenschutzhinweise</a>';
$lang['menu_shipping']='<a href="/versand">Versand</a>';
$lang['menu_my_account']='Mein Konto';
$lang['menu_home']='Home';
$lang['menu_general']='Allgemeines';
$lang['menu_assistance']='Kunden service';

$lang['confirmation-successful'] = 'Die Bestätigung war erfolgreich. Bitte <a href="/login">einloggen</a>.';
$lang['already-confirmed'] = 'Der Benutzer wurde bereits bestätigt. Bitte <a href="%s">  füllen Sie die Registrierung aus. </a>'; //TBC

$lang['contact-thx'] = 'Ihre Nachricht wurde gesendet. Wir setzen uns umgehend mit Ihnen in Verbindung.';
$lang['product-product'] = 'Artikel';
$lang['product-unitprice'] = 'Einzelpreis';
$lang['prod_add_cart']='In den Warenkorb';
$lang['prod_content']='Inhalt der Packung';
$lang['orderno'] = 'Bestell-Nr';
$lang['total_pcs'] = 'Gesamtstücke';
$lang['total_ml'] = 'Gesamtmenge';
$lang['quantity'] = 'Menge';
$lang['subtotal'] = 'Zwischensumme';
$lang['subtotal_abbr'] = '<span title="Zwischensumme">ZS</span>';
$lang['added-cart-msg'] = 'Das Produkt wurde erfolgreich zum Warenkorb hinzugefügt. <a href="/shoppingcart">Zur Kasse</a> oder <a href="/all-products">weiter kaufen </a>';
$lang['back-products'] = 'Zurück zu den Produkten';
$lang['proceed-summary'] = 'Bestellübersicht';  //TBC
$lang['proceed-order'] = 'Bestellen';
$lang['total'] = 'Gesamtpreis';
$lang['shoppingcart'] = 'Warenkorb';
$lang['pieces'] = 'stücke';

$lang['other-packings'] = 'Lieferbar Verpackungen';
$lang['ml'] = 'ml';
$lang['pcs'] = 'Stücke';



//kulcsok kiiratása

/*print_r (array_values($lang));
?><br><br><?php 

//bejárjuk

 $hossz=(sizeof(array_values($lang)));

for($i=0; $i<$hossz; $i++){
    echo str_replace("-","_",array_values($lang)[$i]) .","."<br>";
}; */

//átalakítás sql paranccsá

        //minta
        //minta INSERT INTO `bilingual`.`variables_translation` (`id`, `variable_id`, `language_code`, `description`) VALUES (NULL, '1', 'en', 'Account Details'), 
        //(NULL, '2', 'en', 'User Details');

 $hossz=(sizeof(array_values($lang)));
 
for($i=0; $i<$hossz; $i++){
    echo "(NULL,".($i+1).",'ge','".str_replace("-","_",array_values($lang)[$i]) ."'),"."<br>";
}; 
