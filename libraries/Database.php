
<?php/*
//Absztakt PDO Database működésének leírása: http://culttt.com/2012/10/01/roll-your-own-pdo-php-class/
// konfiguráció meghatározás
	
class Database {
	
    private $host = DB_HOST;
    private $user = DB_USER;
    private $pass = DB_PASS;
    private $dbname = DB_NAME;
 
    private $dbh;
    private $error;
	
	private $stmt;

 
  /*PDO egy adatbázis-elérési absztrakciós réteget biztosít: egy olyan absztrakt programozói felületet,
  mely utasításkészletében nem függ a mögötte álló adatbázis-kezelőtől. Az egyes adatbázisok eléréséhez
  adatbázis-specifikus PDO vezérlőkre van szükség
  
  A PDO a funkcionalitásokat (
    kapcsolatkezelés,
    tranzakciókezelés,
    paraméterezett SQL utasítások és tárolt eljárások kezelése,
    hibakezelés,
    LOB-ok kezelése.
    három osztályon keresztül biztosítja: PDO, PDOStatement és PDOException. 
 


    public function __construct(){
        // adatforrás (DSN)beállítása itt mysqli
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;
    
        // options beállítása
        $options = array(
        	
			PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        );
       
        // PDO példányosítása elvégzi a kapcsolódást az adatbázishoz
        try{
            $this->dbh = new PDO ($dsn, $this->user, $this->pass, $options);
        }
		
        // hibakezelés
        catch(PDOException $e){
            $this->error = $e->getMessage();
        }
    }
    
		// SQL utasítás előkészítése
	
		
	public function query($query){
    $this->stmt = $this->dbh->prepare($query);
}
	
	
	// Adatkötés
	public function bind($param, $value, $type = null){
		if (is_null($type)) {
			switch (true) {
				case is_int($value):
					$type = PDO::PARAM_INT;
					break;
				case is_bool($value):
					$type = PDO::PARAM_BOOL;
					break;
				case is_null($value):
					$type = PDO::PARAM_NULL;
					break;
				default:
					$type = PDO::PARAM_STR;
			}
		}
		$this->stmt->bindValue($param, $value, $type);
	}
	
	// Végrehajtás
	public function execute(){
		return $this->stmt->execute();
	}
	
	// Eredmény beállítása	
	public function resultset(){
		$this->execute();
		return $this->stmt->fetchAll(PDO::FETCH_OBJ);
	}
	
	// Egyke, egy objektumra korlátozza egy osztály létrehozható példányainak számát
	public function single(){
		$this->execute();
		return $this->stmt->fetch(PDO::FETCH_OBJ);
	}
	
	// sorok száma
	public function rowCount(){
		return $this->stmt->rowCount();
	}
	
	// utoljára beszúrt sor azonosítója adatkötés nélküli DML utasítások hívására 
	public function lastInsertId(){
		return $this->dbh->lastInsertId();
	}
	
	
	
	// Tranzakciókezelés
	public function beginTransaction(){
		return $this->dbh->beginTransaction();
	}
	
	public function endTransaction(){
		return $this->dbh->commit();
	}
	
	public function cancelTransaction(){
		return $this->dbh->rollBack();
	}
	
	
/*	// Hibakezelés
	public function debugDumpParams(){
		return $this->stmt->debugDumpParams();
	}*/
	
} 

